package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

var pattern = `{
    "devices": [
      {
        "id": "c2218d37-e386-4ea1-b014-93c9637130d1",
        "actions": [
          {
            "type": "devices.capabilities.on_off",
            "state": {
              "instance": "on",
              "value": true
            }
          },
          {
            "type": "devices.capabilities.color_setting",
            "state": {
                "instance": "scene",
                "value": "%s"
            }
          }          
        ]
      }
    ]
}`

var url = "https://api.iot.yandex.net/v1.0/devices/actions"

type Handler struct {
	*chi.Mux
}

type ObjectAttributes struct {
	Status string `json:"status"`
}

type HTTPRequestBody struct {
	ObjectKind       string           `json:"object_kind"`
	ObjectAttributes ObjectAttributes `json:"object_attributes"`
}

func (h *Handler) ProcessPost(writer http.ResponseWriter, request *http.Request) {
	b, err := io.ReadAll(request.Body)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)

		return
	}

	requestBody := &HTTPRequestBody{}

	err = json.Unmarshal(b, requestBody)
	if err != nil {
		log.Printf("Error: %v", err)
	}

	log.Printf("status: %v", requestBody.ObjectAttributes.Status)

	body := ""

	log.Printf("state: %v", requestBody.ObjectAttributes.Status)
	switch requestBody.ObjectAttributes.Status {
	case "failed":
		body = fmt.Sprintf(pattern, "alarm")
	case "success":
		body = fmt.Sprintf(pattern, "jungle")
	default:
		body = fmt.Sprintf(pattern, "candle")
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer([]byte(body)))
	if err != nil {
		log.Printf("Error: %v", err)
	}

	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", os.Getenv("AUTH_TOKEN")))
	client := &http.Client{}

	resp, err := client.Do(req)

	log.Printf("Response: %v", resp)

	_, err = writer.Write([]byte("ok"))
	if err != nil {
		log.Printf("Error: %v", err)
	}
}

func NewHandler() *Handler {
	h := &Handler{
		Mux: chi.NewMux(),
	}

	h.Use(middleware.Logger)
	h.Use(middleware.RealIP)

	h.Post("/webhook", h.ProcessPost)

	return h
}

func main() {
	log.Fatal(http.ListenAndServe(":8089", NewHandler()))
}
